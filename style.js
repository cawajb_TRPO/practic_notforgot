module.exports = {
    colors: {
        statusBarColor: 'black',
        accent: 'orange',
        background: 'white',
        priority: {
            1: 'green',
            2: 'blue',
            3: 'yellow',
            4: 'red',
        },
        text: {
            primary: 'black',
            secondary: '#555',
        },
    },
};
