import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

const style = require('../style');

const Header = () => {

    return (
        <View style={styles.headerView}>
            <Text style={styles.text}>
                NotForgot Кудаяров
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    headerView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,
        backgroundColor: style.colors.accent,
    },
    text: {
        fontSize: 16,
        color: '#FFF',
    },
});

export default Header;
