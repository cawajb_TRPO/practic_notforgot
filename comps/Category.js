import React, {useState} from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    Animated,
    TouchableHighlight,
    TouchableOpacity,
    StatusBar,
    Button,

} from 'react-native';
import {SwipeListView} from 'react-native-swipe-list-view';

import CheckBox from '@react-native-community/checkbox';

const style = require('../style');

const Category = (props) => {
    const [isSelected, setSelection] = useState(false);
    return (
        <View style={styles.mainView}>
            <Text style={styles.categoryName}>{props.name}</Text>
            {props?.tasks?.map((task, index) => (
                <View key={index} style={styles.taskWrapper}>
                    <View style={{
                        width: 5,
                        height: '100%',
                        marginRight: 5,
                        backgroundColor: style.colors.priority[task.priority],
                    }}/>

                    <View style={styles.taskContentWrapper}>
                        <View style={styles.taskTitleDesc}>
                            <Text style={styles.taskTitle}>{task.title}</Text>
                            <Text style={styles.taskDesc}>{task.desc}</Text>
                        </View>
                        <View>

                            <CheckBox

                            />
                            <CheckBox
                                style={styles.delItems} onChange={delValue(props.name,index)}
                            />
                        </View>
                    </View>
                </View>

            ))}

        </View>
    );
    function delValue(name,key) {

    }
};

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        padding: 12,
        paddingLeft: 0,
    },
    categoryName: {
        fontSize: 22,
        color: style.colors.text.primary,
        marginBottom: 12,
        paddingLeft: 20,
    },
    taskWrapper: {
        flexDirection: 'row',
    },
    taskContentWrapper: {
        width: '97%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    delItems: {

        backgroundColor: 'red',
    },
    taskTitleDesc: {
        paddingVertical: 8,
        maxWidth: '85%',
    },
    taskTitle: {
        color: style.colors.text.primary,
        fontSize: 18,
    },
    taskDesc: {
        fontSize: 16,
        color: style.colors.text.secondary,
    },
});

export default Category;
