import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    Button,
    Modal,
    Alert,
    TextInput, SafeAreaView,
} from 'react-native';
import Header from './Header';


class NewTask extends React.Component {
    constructor() {
        super();
        this.state = {
            show: false,
        };
    }

    render() {
        /*const [priority,setPriority] = React.useState('1');
        const description = React.useState('');
        const nameTask = React.useState('');
        const categoryTask = React.useState('Бытовуха');*/
        return (


            <View>
            <Button
                title="new task"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"

                onPress={() => this.setState({show: !this.state.show})}
            />
            <Modal
                transparent={true}
                visible={this.state.show}
            >
                <View
                    style={{backgroundColor: '#000', flex: 1}}
                >
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic">
                        <View
                            style={{backgroundColor: '#fff', margin: 10, padding: 10, borderRadius: 10, flex: 1}}
                        >
                            <Text>Введите категорию (из имеющихся)</Text>
                            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}} />

                            <Text>Введите имя</Text>
                            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}}/>

                            <Text>Введите описание</Text>
                            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}} />

                            <Text>Введите важность</Text>
                            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                                       placeholder={'1-4'}/>
                            <View

                                style={{paddingTop: 20}}
                            >
                                <Button
                                    title="Добавить"
                                    color="#841584"
                                    accessibilityLabel="Learn more about this purple button"
                                    onPress={addNew}
                                /></View>

                        </View>
                    </ScrollView>
                </View>
                <Button
                    title="back"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                    onPress={() => this.setState({show: !this.state.show})}
                />
            </Modal>

        </View>);

        function addNew() {
            /*if (name !== '' && description !== '' && (priority === '1' && priority === '2' && priority === '3' && priority === '4') && (categoryTask === 'Работа' && categoryTask === 'Учёба' && categoryTask === 'Бытовуха')) {

            }*/
        }
    }

}


const styles = StyleSheet.create({
    categoryName: {
        position: 'absolute',
        left: 0,
        top: 0,
    },
});

export default NewTask;
