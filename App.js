import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';


import Header from './comps/Header';
import Category from './comps/Category';
import NewTask from './comps/NewTask';

const style = require('./style');
const data = require('./data');
const App: () => React$Node = () => {
  return (
      <>
        <StatusBar backgroundColor={style.colors.statusBarColor}/>
        <SafeAreaView style={styles.screenWrapper}>
          <Header />
          <ScrollView
              contentInsetAdjustmentBehavior="automatic"
              style={styles.scrollView}>
            {data.categories.map((category, index) => (
                <Category key={index} name={category.name} tasks={category.tasks}/>
            ))}
          </ScrollView>
          <NewTask />
        </SafeAreaView>
      </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: style.colors.background,
  },
  screenWrapper: {
    maxHeight: "100%"
  }

});

export default App;
